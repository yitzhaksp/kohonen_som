import matplotlib.pyplot as plt
import numpy as np



data1 = np.random.multivariate_normal([0,0], [[1, 0], [0, 1]], 100)
data2 = np.random.multivariate_normal([7,1],[[1, 1.3], [0, 1]] , 100)
data=np.concatenate((data1,data2),axis=0)
data=normalize(data)
plt.plot(data[:,0], data[:,1], 'x')
plt.axis('equal')
plt.show()