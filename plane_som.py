import numpy as np
from misc_utils import *
import matplotlib.pyplot as plt
np.random.seed(1)

num_points=100
data_dim = 2
range_max = som_rows + som_cols
learn_max = 0.5
shape='line'

som_rows = 5; som_cols = 5
som=np.random.rand(som_rows,som_cols,data_dim)
if shape == 'line':
    data=np.random.rand(num_points,2)
    data[:,1]= .2 + .3*data[:,0]
elif shape == 'gaussian':
    data = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], num_points)
    data = normalize(data)
elif shape == 'gaussians':
    data1 = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], num_points)
    data2 = np.random.multivariate_normal([7, 1], [[1, 1.3], [0, 1]], num_points)
    num_points*=2
    data = np.concatenate((data1, data2), axis=0)
    data = normalize(data)

plt_stages=[0,5,10,20,30,50,100,200]
steps_max=max(plt_stages)+1
plt_num=1
for s in range(steps_max):
    pct_left = 1.0 - (s / steps_max)
    curr_range = (int)(pct_left * range_max)
    curr_rate = pct_left * learn_max
    point_ind = np.random.randint(num_points)
    (bmu_row, bmu_col) = closest_node(data[point_ind], som)
    if s in plt_stages:
        plt.subplot(2,4,plt_num)
        plt.scatter(som[:, :, 0], som[:, :, 1], marker='s',s=5.0)
        plt.scatter(data[:, 0], data[:, 1], s=3.0)
        plt.title('k='+str(s))
        plt_num+=1
    for i in range(som_rows):
        for j in range(som_cols):
            if manhattan_dist((bmu_row, bmu_col), (i, j)) < curr_range:
                som[i,j] = som[i,j] + curr_rate * (data[point_ind] - som[i,j])

plt.show()
tmp=7