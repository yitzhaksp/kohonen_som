import numpy as np

def normalize(X):
    return (X-X.min(0))/X.ptp(0) #normalize

def closest_node(point,data):
    closest=(0,0)
    min_dist2=np.linalg.norm(point-data[0,0])
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            dist2 = np.linalg.norm(point - data[i,j])
            if dist2 < min_dist2:
                min_dist2=dist2
                closest=(i,j)
    return closest

def manhattan_dist(x,y):
    return abs(x[0]-y[0]) + abs(x[1]-y[1])

